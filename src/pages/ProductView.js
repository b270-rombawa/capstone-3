import React, { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);

  //allows us to retrieve name passed via the URL
  const { name } = useParams();

  	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

  const [product, setProduct] = useState(null);

  const AddCart = (productId) => {
    const data = {
      userId: user.id,
      productId: productId
    };

    fetch(`${process.env.REACT_APP_API_URL}/users/placeOrder`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(data)
    })
      .then(res => {
        if (!res.ok) {
          throw new Error("Error adding to cart. Please try again.");
        }
        return res.json();
      })
      .then(data => {
        console.log(data);
        Swal.fire({
          title: "Added to Cart!",
          icon: "success",
          text: "You have added this product to your cart."
        });
        navigate("/products");
      })
      .catch(error => {
        Swal.fire({
          title: "OOPS! Something went wrong",
          icon: "error",
          text: "Admins cannot add to cart"
        });
      });
  };

  useEffect(() => {
    const fetchProductDetails = async () => {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${name}`);
        if (!response.ok) {
          throw new Error("Error fetching product details.");
        }
        const data = await response.json();
        console.log(data);
        setProduct(data);
      } catch (error) {
        Swal.fire({
          title: "OOPS! Something went wrong",
          icon: "error",
          text: error.message
        });
      }
    };

    fetchProductDetails();
  }, [name]);

  return (
    <Container>
      <Row>
        <Col>
          {product && (
            <Card>
              <Card.Body className="text-center">
                <Card.Title>{product.name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{product.description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {product.price}</Card.Text>
                {user.id !== null ? (
                  <Button variant="primary" onClick={() => AddCart(product.productId)}>Add to Cart</Button>
                ) : (
                  <Button variant="danger" as={Link} to="/login">Add to Cart</Button>
                )}
              </Card.Body>
            </Card>
          )}
        </Col>
      </Row>
    </Container>
  );
}

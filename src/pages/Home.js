import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import './Home.css';

export default function Home() {
  const data = {
    title: "Shopee x Spy",
    content: "Discover a world where cuteness meets espionage.",
    destination: "/products",
    label: "Shop Now!"
  };

  return (
    <>
      <div className="banner-container" style={{ backgroundColor: '#8da99b' }}>
        <Banner data={data} />
      </div>
      <div className="highlights-container">
        <Highlights />
      </div>
    </>
  );
}
